Hi!, thanks for visiting us <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">

# SQI datasets


Documentation for SQI datasets of sEMG signals in deglutition. This repository contains three datasets generated in the research article accesible in [DOI: 10.1016/j.bspc.2021.103122](https://doi.org/10.1016/j.bspc.2021.103122)


![Visitors](https://visitor-badge.glitch.me/badge?page_id=Domadrona.sqi-dataset&left_color=red&right_color=green)

You can find me on [hotmail](mailto:julio-jimmy@hotmail.com), [gmail](mailto:juliojimmy1695@hotmail.com) or in my [institucional mail](mailto:juliocuadros230835@correo.itm.edu.co)

![](https://img.shields.io/twitter/follow/julio_jimmy?style=social)
![](https://img.shields.io/github/followers/Domadrona?style=social)

## Contents

- [Datasets description](#how-to-use-this-guide)
- [Versions of the implemented software](#requirements)
- [How to cite](#how-to-cite)
- [License](#license)


## Datasets description


The datasets 1 and 2 were extracted form the database built in the project “Diagnóstico y seguimiento de pacientes con disfagia neuromuscular y neurogénica mediante la integración de señales no invasivas y variables clínicas” (Diagnosis and monitoring of patients with neuromuscular and neurogenic dysphagia through the integration of non-invasive signals and clinical variables). Proyect developed in the Gi2B Biomedical Research and Innovation Group of the Instituto Tecnológico metropolitano - Medellín. The project was executed within the framework of the call for projects of science, technology and innovation in health of the Ministry of Science, Technology and Innovation Colciencias.

The acquisition of surface electromyography signals in both datasets was performed using adhesive electrodes at 2kHz sampling frequency. A 10-500Hz analog bandpass filter was applied. Further description can be accesible in the research article by Roldan-Vasco et. al (Automatic detection of oral pharyngeal phases in swallowing using classification algorithms and multichannel EMG, 2018)  [DOI: 10.1016/j.jelekin.2018.10.004](https://doi.org/10.1016/j.jelekin.2018.10.004)

Dataset 3 was extracted from a database described by Garcia-Casado et. al (Evaluation of swallowing realted muscle activity by means of concentric ring electrodes, 2020) [DOI: doi.org/10.3390/s20185267](https://doi.org/10.3390/s20185267)
The acquisition was performed using adhesive electrodes at 5kHz sampling frequency. A 1-1000Hz analog bandpass filter, and a 50Hz notch filter were applied.

The datasets can be loaded in four formats including .mat(cell), .pickle, .csv, mat(struct). For example, to open the file dataset_1.pkl the line `df_loaded = pd.read_pickle("dataset_1.pkl")` does the magic.

![](dataframe.png)

<details><summary>Click to expand the description of acronyms</summary>




## The protocol included the swallowing of 3 different boluses and saliva, as follows:


A = Water

G = Cookie

Y = Yogurt

S = Saliva

Numbers 5, 10 and 20 refers to milliliters

## The sEMG channels were recorded by muscle group as follows:

RM  = Right Masseter

LM  = Left Masseter

RSH = Right suprahyoid

LSH = Left suprahyoid

RIH = Right infrahyoid 

LIH = Left Infrahyoid

OB  = orbicular of lips


</details>



## Versions of the implemented software
1. Python 3.8.3 on Spyder 4.1.5

2. Scypi 1.5.0

3. Pandas 1.1.1

4. Matlab R2020a

## How to cite
If you use this datasets, please cite like this:

```bibtex
@article{J.Cuadros-Acosta,
  author       = J.Cuadros-Acosta and  A.Orozco-Duque,
  title        = Automatic detection of poor quality signals as a pre-processing scheme in the analysis of sEMG signals in swallowing,
  year         = 2022,
  ISSN         = 1746-8094,
  publisher    = Biomedical Signal Processing and Control - Volume 71 - Part A,
  doi          = 10.1016/j.bspc.2021.103122,
  url          = https://doi.org/10.1016/j.bspc.2021.103122
}
```
## License
---License Under development---
